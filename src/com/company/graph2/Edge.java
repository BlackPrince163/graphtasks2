package com.company.graph2;

public class Edge implements Comparable<Edge> {
    int weight;
    int first;
    int second;


    public Edge(int weight, int first, int second) {
        this.weight = weight;
        this.first = first;
        this.second = second;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getWeight() {
        return weight;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public int compareTo(Edge o) {
        return this.weight - o.getWeight();
    }
}
