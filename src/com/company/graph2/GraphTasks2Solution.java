package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        return null;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int[] line;

        int point = 0;
        int total = 0;
        Set<Integer> dots = new HashSet<>();
        dots.add(point);
        int sense;
        while(dots.size() != adjacencyMatrix.length)
        {
            int x = 0, y = 0;
            int min = 1000000;
            for(int i : dots)
            {
                line = adjacencyMatrix[i];
                for(int j = 0; j < line.length; j++)
                {
                    sense = line[j];
                    if((sense < min) && (sense != 0) && (!dots.contains(j)))
                    {
                        x = i;
                        y = j;
                        min = sense;
                    }
                }

            }
            dots.add(y);
            dots.add(x);
            total += min;
        }

        return total;
    }


    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Edge> edges = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = i; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    edges.add(new Edge(i, j, adjacencyMatrix[i][j]));
                }
            }
        }
        Collections.sort(edges);
        ArrayList<Integer> vertex = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertex.add(i);
        }
        int total = 0;
        for (Edge edge : edges) {
            int weight = edge.getWeight();
            int start = edge.getFirst();
            int end = edge.getSecond();
            if (!vertex.get(start).equals(vertex.get(end))) {
                total = total + weight;
                int a = vertex.get(start);
                int b = vertex.get(end);
                for (int i = 0; i < adjacencyMatrix.length; i++) {
                    if (vertex.get(i) == b) {
                        vertex.set(i, a);
                    }
                }
            }
        }
        return total;
    }

}
